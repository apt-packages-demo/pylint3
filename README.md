# pylint3

Python code static checker and UML diagram generator

* https://tracker.debian.org/pkg/pylint

## License
* https://github.com/PyCQA/pylint/blob/master/COPYING

## See also
* https://pkgs.alpinelinux.org/packages?name=\*pylint\*
